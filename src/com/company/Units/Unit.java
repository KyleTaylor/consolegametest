package com.company.Units;

import com.company.Drawer.Drawer;
import com.company.Functions.*;

public class Unit {

    Transform transform = new Transform();

    private Transform myPosition = new Transform();

    //-----------------------Functions-----------------------//

    public Transform SetPosition(int x, int y) {

        transform = transform.Vector2( x, y);
        myPosition = transform;

        return transform;
    }

    public Transform MyPosition(){
        return myPosition;
    }

    public void Create(Transform position){
        transform = position;
        //unit
    }

}
