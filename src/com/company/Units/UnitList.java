package com.company.Units;

import com.company.Functions.Transform;
import com.company.Units.Enemies.RedBlob;

import java.util.ArrayList;

public class UnitList {

    Unit unit = new Unit();
    Player player = new Player();
    RedBlob redBlob = new RedBlob();

    private ArrayList<RedBlob> redBlobs = new ArrayList<>();
    private ArrayList<Unit> units = new ArrayList<>();

    //-----------------------Functions-----------------------//

    {
        //Check unit list
        for (Unit element : units) {
            //Check if unit is a RedBlob & unit is not in redBlobs list
            if (element.getClass() == RedBlob.class && !redBlobs.contains(element)){
                //Add to temp blob list
                redBlobs.add((RedBlob) element);
            }
        }
        //Add
        //Player
        if (units.contains(player)){
        }else
        units.add(0,player);
    }

    public void NewRedBlob(int amount){
        for (int i = 0; i < amount; i++) {
            //Creates blob
            RedBlob blob = new RedBlob();
            //Adds same blob to both lists
            redBlobs.add(0, blob);
            units.add(0, blob);
        }
    }

    public ArrayList<Unit> GetUnitList(){
        return units;
    }

}
