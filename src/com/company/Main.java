package com.company;

import com.company.Drawer.Graph;

public class Main {

    static Graph graph = new Graph();

    public static void main(String[] args) {
        //GraphSize
        GraphSize(12,6);
        //Draws graph
        DrawGraph();
    }
    public static void GraphSize(int x, int y){
        graph.SetGraphSize(x,y);
    }

    public static void DrawGraph(){
        graph.Graph();
    }

}
