package com.company.Drawer;

import com.company.Functions.Transform;
import com.company.Units.Unit;
import com.company.Units.UnitList;

import java.util.ArrayList;

public class Drawer {

    public static class Color {
        //---------------Colors---------------//
        public static final String base = "\u001B[0m";

        private static final String reset = "\u001B[0m";
        private static final String black = "\u001B[30m";
        private static final String red = "\u001B[31m";
        private static final String green = "\u001B[32m";
        private static final String yellow = "\u001B[33m";
        private static final String blue = "\u001B[34m";
        private static final String purple = "\u001B[35m";
        private static final String cyan = "\u001B[36m";
        private static final String white = "\u001B[37m";
    }

    Color color = new Color();

    //Coordinate
    Transform transform = new Transform();
    ArrayList<Unit> unitLists = new UnitList().GetUnitList();

    private int positionX = 1;
    private int positionY = 1;



    //-----------------------Functions-----------------------//

    public Transform GetPosition(){
        return transform.Vector2(positionX, positionY);
    }

    //Draws Vertical line with Roof, Floor & looping contents
    public void DrawGraph(int x, int y){
        //Draw Roof
        DrawHorizontalLine(x);
        //Draw Play Space
        for (int i = 0; i < y; i++) {
            DrawHorizontal(x);
        }
        //Draw Floor
        DrawHorizontalLine(x);
    }
    //Draws a "------------------", length based on input 'x'
    private void DrawHorizontalLine(int x){
        for (int i = 0; i < x+1 ; i++) {              //Add +1 '-' for Left Wall
            Draw(Color.white, "- ");
        }
        Draw(Color.white, "-");                 //Add +1 '-' for Right Wall
        BlankRow(1);
    }

    //Draws Horizontal line with 2 Walls & looping contents
    public void DrawHorizontal(int x){
        //Draw Left Wall
        Draw(Color.white, "| ");
        for (int i = 0; i < x; i++) {
            //Draw Unit
            DrawUnit();
        }
        //Draw Right Wall
        Draw(Color.white, "|");

        //Next Row
        positionX = 1;
        positionY +=1;
        BlankRow(1);
    }

    private void DrawUnit(){
//            //Check what to place
//            if(transform.Vector2(positionX, positionY) == unitLists.get()){
//
//            }
//            //Empty Spot
//            else
            Draw(". ");
        //Update transform
        positionX += 1;
    }

    public void BlankRow(int amount){
        if (amount > 0)
        for (int i = 0; i < amount; i++) {
            System.out.println("");
        }
    }

    private void Draw(String icon){
        System.out.print(Color.base + icon + Color.reset);
    }
    private void Draw(String color, String icon){
        System.out.print(color + icon + Color.reset);
    }
    private void DrawLine(String icon){
        System.out.println(Color.base + icon + Color.reset);
    }
    private void DrawLine(String color, String icon){
        System.out.println(color + icon + Color.reset);
    }

}
