package com.company.Drawer;

import com.company.Drawer.Drawer;
import com.company.Functions.Functions;
import com.company.Functions.Transform;

import javax.xml.stream.Location;
import java.util.ArrayList;
import java.util.Locale;


public class Graph extends Drawer{


    Drawer drawer = new Drawer();
    Functions function = new Functions();
    Transform transform = new Transform();

    private int xLength;
    private int yLength;

    private ArrayList<Transform> vectors = new ArrayList<Transform>();

    public void SetGraphSize(int horizontalLength, int verticalLength){
        xLength = horizontalLength;
        yLength = verticalLength;
    }

    public void Graph(){
        System.out.println("Play Area: ("+xLength + ", " + yLength+")");

        drawer.DrawGraph(xLength, yLength);
    }

    public void FirstDrawGraph(){

    }

}
